package model

/*
All structurs:
CabinetOfPS
Cars
CarsOfCabinet
Cities
ClientsOfDTrip
ColorOfAuto
Dialogs
Freight
HistoryOfRent
InCityOfTrip
Mark
OutCityOfDTrip
OutCityOfTrip
PromoCOd
ServicesOfPS
SessionsOfDR
Tokens
TransactionsOfDR
TransactionsOfPS
TripInfo
TripStatus
Users
*/

//ScarKey - keep key for SCAR server
type ScarKey struct {
	ID         string `json:"id"`
	KeyForSCAR string `json:"key"`
}

//ScarToken - keep key for SCAR server
type ScarToken struct {
	ID    string `json:"id"`
	Token string `json:"token"`
}

// //CabinetOfPS -
// type CabinetOfPS struct {
// 	ID string `json:"id"`
// }

// //Cars -
// type Cars struct {
// 	ID string `json:"id"`
// }

// //CarsOfCabinet -
// type CarsOfCabinet struct {
// 	ID string `json:"id"`
// }

// //Cities -
// type Cities struct {
// 	ID string `json:"id"`
// }

// //ClientsOfDTrip -
// type ClientsOfDTrip struct {
// 	ID string `json:"id"`
// }

// //ColorOfAuto -
// type ColorOfAuto struct {
// 	ID string `json:"id"`
// }

// //Dialogs -
// type Dialogs struct {
// 	ID string `json:"id"`
// }

// //Freight -
// type Freight struct {
// 	ID string `json:"id"`
// }

// //HistoryOfRent -
// type HistoryOfRent struct {
// 	ID string `json:"id"`
// }

// //InCityOfTrip -
// type InCityOfTrip struct {
// 	ID string `json:"id"`
// }

// //Mark -
// type Mark struct {
// 	ID string `json:"id"`
// }

// //OutCityOfDTrip -
// type OutCityOfDTrip struct {
// 	ID string `json:"id"`
// }

// //OutCityOfTrip -
// type OutCityOfTrip struct {
// 	ID string `json:"id"`
// }

// //PromoCOd -
// type PromoCOd struct {
// 	ID string `json:"id"`
// }

// //ServicesOfPS -
// type ServicesOfPS struct {
// 	ID string `json:"id"`
// }

// //SessionsOfDR =
// type SessionsOfDR struct {
// 	ID string `json:"id"`
// }

// //Tokens -
// type Tokens struct {
// 	ID string `json:"id"`
// }

// //TransactionsOfDR -
// type TransactionsOfDR struct {
// 	ID string `json:"id"`
// }

// //TransactionsOfPS -
// type TransactionsOfPS struct {
// 	ID string `json:"id"`
// }

// //TripInfo -
// type TripInfo struct {
// 	ID string `json:"id"`
// }

// //TripStatus -
// type TripStatus struct {
// 	ID string `json:"id"`
// }

// //Users -
// type Users struct {
// 	ID string `json:"id"`
// }
