/*
Client model for client API.
Author: GuselnikovVasiliy/BookerDevid mr.wgw@yandex.ru

all word:
id - индентификатор
tok - token - токен
phe	- phone - телефон
rtg - rating -рэйтинг
cod - code verification - код подтверждения
dte - date - дата/время
nme - name - имя
sme - surname - фамилия
mme - middlename - отчество
usr - user - пользователь
cnt - contact - контакт
msg - message - сообщение
cty - city - город
nmc - full name city - полное наименование города
nen - name city of english - наименование на английском
nsh - short name city - короткое название города
lat - latitude - долгота
lon - longitude - широта
ord - order - заказ
por - plan order - запланированный заказ
tpl - time plan - время планирования
trp - trip - поездка
plc - place - место
drv - driver - водитель
car - автомобиль
clr - color - цвет
mrk - mark - марка
cmt - comment - комментарий
prc - price - цена
sta - status - статус
num - number - номер
sex - sex - пол
img - image get - ссылка для загрузки фотографии
imp - image put - ссылка здя выгрузки фотографии
yrs - years - возраст
smk - smoking - курение в солоне
fup - fast up auto - быстрая подача автомобиля
pay - pay system - система оплаты
bby - babych - детское кресло
key - смс ключ
tit - title - заголовок
val - value - содержимое
hon - housenumber - номер дома
str - street - улица
typ - type - тип
prt - Partner
ani - animal - животное
opt - option - параметры
reg - region - регион
pch - porch - подъезд
cme - comment exist - существования комментария
ver - verification - проверка
bgn-dte - begin date - дата начала
end-dte - end date - дата окончания
doc-num - document number - номер документа
*/

package model

//Token - model for update or load token of the client
type Token struct {
	Token string `json:"tok"`
}

//City - model for update or load of th cities
type City struct {
	City []CityModel `json:"cty"`
}

//GetUserData - Take user data
type GetUserData struct {
	Token         string    `json:"tok"`
	Driver        bool      `json:"drv"`
	DriverProfile bool      `json:"drv-prf"`
	Phone         string    `json:"phe"`
	UserName      string    `json:"nme"`
	City          CityModel `json:"cty"`
	Rating        int       `json:"rtg"`
	DateB         string    `json:"dte"`
	Sex           bool      `json:"sex"`
	ImageStatus   string    `json:"img"`
}

//CityModel - Take data of city
type CityModel struct {
	NameCity   string  `json:"name-city"`
	NameCityEn string  `json:"name-en"`
	NameCitySh string  `json:"name-short"`
	Lat        float64 `json:"lat"`
	Lon        float64 `json:"lon"`
	Price      int     `json:"price"`
}

/*
Session data - it's base data for client app.
*/

//SessionData - Struct for loading data of client
type SessionData struct {
	Token       string      `json:"token"`
	LastSession bool        `json:"last-session"`
	General     GeneralData `json:"general"`
	Driver      DriverData  `json:"driver"`
}

//GeneralData - Data for master object Client
type GeneralData struct {
	Phone       string            `json:"phone"`
	UserName    string            `json:"username"`
	Rating      int               `json:"rating"`
	DateB       string            `json:"date-bir"`
	Sex         bool              `json:"sex"`
	ImageStatus string            `json:"img"`
	City        CityModel         `json:"city"`
	Orders      []OrderClientData `json:"orders"`
}

//DriverData - Data for slave object Driver
type DriverData struct {
	Exist        bool     `json:"exist"`
	Name         string   `json:"username"`
	Surname      string   `json:"surname"`
	Middlename   string   `json:"middle-name"`
	Verification bool     `json:"verification"`
	Car          CarData  `json:"car"`
	Rent         RentData `json:"rent"`
}

//CarData - Data auto
type CarData struct {
	Exist        bool   `json:"exist"`
	Color        string `json:"color"`
	Mark         string `json:"mark"`
	NumReg       string `json:"reg-number"`
	Verification bool   `json:"confirmed"`
}

//RentData - Data rent auto add parameters idCabinet and Name Cabinet
type RentData struct {
	IDCabinet   string  `json:"cbn"`
	NameCabinet string  `json:"nme"`
	Car         CarData `json:"car"`
}

//OrderClientData - Data order for client
type OrderClientData struct {
	ID        string     `json:"id"`
	Status    string     `json:"status"`
	InCity    bool       `json:"in-city"` //True - in City/False - out City
	Places    []Place    `json:"places"`
	AddOption OptionData `json:"parametrs"`
}

//OrderDriverData - Data order for driver
type OrderDriverData struct {
	ID        string     `json:"id"`
	Status    string     `json:"status"`
	InCity    bool       `json:"in-city"` //True - in City/False - out City
	Places    []Place    `json:"places"`
	AddOption OptionData `json:"parametrs"`
}

//OptionData  - for OrderModelForSend
type OptionData struct {
	Price     int    `json:"price"`
	Babych    bool   `json:"babych"`
	Smoking   bool   `json:"smoking"`
	FastUp    bool   `json:"fast-up"`
	Comment   string `json:"comment"`
	Animal    bool   `json:"animals"`
	PlanOrder bool   `json:"plan_order"`
	PlanTime  int64  `json:"date_plan"`
	SystemPay bool   `json:"pay"`
}

//PlaceData - Data Place for order
type PlaceData struct {
	City      string  `json:"city"`
	Addr      string  `json:"addr"`
	TypePorch int     `json:"type_orch"`
	Porch     int     `json:"porch"`
	Lat       float64 `json:"lat"`
	Lon       float64 `json:"lon"`
}
