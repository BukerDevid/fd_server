package order

import (
	"fmt"

	"../model"
)

//CheckLocationOrder - check location order for create order on type is True or False
func CheckLocationOrder(places []model.Place) (bool, error) {
	fmt.Println("Check location order")
	CityStart := ""
	for idx, place := range places {
		fmt.Printf(" Place %v:\nCity - %s\n Address - %s %v %v\n Lat - %v, Lon - %v",
			idx+1,
			place.City,
			place.Addr,
			place.TypePorch,
			place.Porch,
			place.Lat,
			place.Lon)
		if idx != 0 && CityStart != place.City {
			return false, nil
		} else {
			CityStart = place.City
		}
	}
	return true, nil
}

//CheckOrderData - проверка данных заказа
/*
type:
- create 	- создание заказа (if order not exist)

- load 		- загрузка закака (if status: driver_add, ...,  end_trip)
- start		- начать заказ (if status: plan_out)

status:
- create -> (live 2 min) || plan (live time_duration + 2 min)
- plan_add_driver
- plan_del_driver
- plan_out -> (live 2 min)
- start (live 2 min woth checking client FireBase)
- driver_add -> (live 5 min)
- driver_ready -> (live 5 min)
- driver_out_time -> (live 5 min)
- begin_trip -> (live 5 min)
- end_trip
- end
- cancel
*/
func CheckOrderData(modelClient *model.SocketClient) (bool, []model.Place, model.OptionModel, error) { //
	inCity, err := CheckLocationOrder(modelClient.Order.Places) //True - in City/False - out City
	if err != nil {
		return false, modelClient.Order.Places, modelClient.Order.AddOption, err
	}
	return inCity, modelClient.Order.Places, modelClient.Order.AddOption, nil
}

//DumpDataOrder -
func DumpDataOrder(modelClient *model.OrdermodelforCreate) (bool, []model.Place, model.OptionModel, error) { //
	inCity, err := CheckLocationOrder(modelClient.Places) //True - in City/False - out City
	if err != nil {
		return false, modelClient.Places, modelClient.AddOption, err
	}
	return inCity, modelClient.Places, modelClient.AddOption, nil
}
