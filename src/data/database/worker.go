package database

import (
	"context"
	"fmt"
	"sync"
	"time"
)

//WorkersObject - local cache for storeg
type WorkersObject struct {
	orderStateCreated     stateCreated     // _time_live_order_creted
	orderStatePlan        statePlan        // _time_waiting_plan
	orderStateStart       stateStart       // _time_waiting_driver
	orderStateDriverReady stateDriverReady // _time_waiting_client
	orderStateBeginTrip   stateBeginTrip   // _time_duration_live_trip
	queueOnCancel         onCancel         // _time_duration_out_time_Order
	orderStateEnd         stateEnd         // check rating
	timing                cities
}

type cities struct {
	sync.Mutex
	city map[string]timeCity
}

type timeCity struct {
	timeLiveOrderCreated int
	timeWaitingClient    int
	timeLiveOrderTrip    int
	timeWaitingCheck     int
	timeWaitingDriver    int
}

type stateCreated struct {
	sync.Mutex
	value map[string]orderData
}

type statePlan struct {
	sync.Mutex
	value map[string]orderData
}

type stateStart struct {
	sync.Mutex
	value map[string]orderData
}

type stateDriverReady struct {
	sync.Mutex
	value map[string]orderData
}

type stateBeginTrip struct {
	sync.Mutex
	value map[string]orderData
}

type onCancel struct {
	sync.Mutex
	value map[string]orderData
}

type stateEnd struct {
	sync.Mutex
	value map[string]orderData
}

type orderData struct {
	city      string
	time      int64
	lastState string
}

//NewWorkerObject - create new storeg WorkerObject's
func NewWorkerObject() *WorkersObject {
	return &WorkersObject{
		orderStateCreated:     stateCreated{value: make(map[string]orderData)},
		orderStatePlan:        statePlan{value: make(map[string]orderData)},
		orderStateStart:       stateStart{value: make(map[string]orderData)},
		orderStateDriverReady: stateDriverReady{value: make(map[string]orderData)},
		orderStateBeginTrip:   stateBeginTrip{value: make(map[string]orderData)},
		queueOnCancel:         onCancel{value: make(map[string]orderData)},
		orderStateEnd:         stateEnd{value: make(map[string]orderData)},
	}
}

//StartWorkers - controll timing and live orders
func (db *Database) StartWorkers() {
	fmt.Println("Start Worker")
	go db.workerOrder.workerStateCreated(db)
	go db.workerOrder.workerStatePlan(db)
	go db.workerOrder.workerStateStart(db)
	go db.workerOrder.workerStateDriverReady(db)
	go db.workerOrder.workerStateBeginTrip(db)
	go db.workerOrder.workerStateEnd(db)
	go db.workerOrder.workerQueueOnCancel(db)
}

func (wo *WorkersObject) workerStateCreated(db *Database) {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	fmt.Println("Start worker - @StateCreated")
	for {
		time.Sleep(time.Second * 5)
		createdOrder := wo.GetStepOrdersWithStateCreated()
		if len(createdOrder) == 0 {
			// fmt.Println("@StateCreated - empty")
			continue
		}
		unix := time.Now().Unix()
		fmt.Println("\n@StateCreated - list:")
		for idx, value := range createdOrder {
			fmt.Printf("%v time out - %v | %v\n", idx, value.time, time.Now().Unix())
			if value.time < unix {
				wo.DelStepOrdersWithStateCreated(idx)
				timingLocalCity := wo.GetTimingCity()
				fmt.Println("Time list and waite data - ", timingLocalCity[value.city].timeWaitingCheck)
				timeLive := time.Now().Unix() + int64(timingLocalCity[value.city].timeWaitingCheck)
				wo.SetStepOrdersWithStateOnCancel(idx, timeLive, value.city, "created")
				dbConn.Exec(context.Background(), "SELECT pg_notify($1, $2)", "orders", fmt.Sprintf("%v*%v*%v", "time_out", idx, "created")) //action time_out
			}
		}
	}
}

func (wo *WorkersObject) workerStatePlan(db *Database) {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	fmt.Println("Start worker - @StatePlan")
	for {
		time.Sleep(time.Second * 5)
		planOrder := wo.GetStepOrdersWithStatePlan()
		if len(planOrder) == 0 {
			// fmt.Println("@StatePlan - empty")
			continue
		}
		unix := time.Now().Unix()
		for idx, value := range planOrder {
			if value.time < unix {
				wo.DelStepOrdersWithStatePlan(idx)
				timingLocalCity := wo.GetTimingCity()
				fmt.Println("Time list and waite data - ", timingLocalCity[value.city].timeWaitingCheck)
				wo.SetStepOrdersWithStateOnCancel(idx, time.Now().Unix()+int64(timingLocalCity[value.city].timeWaitingCheck), value.city, "plan")
				dbConn.Exec(context.Background(), "SELECT pg_notify($1, $2)", "orders", fmt.Sprintf("%v*%v*%v", "tume_out", idx, "plan")) //action plan_out
			}
		}
	}
}

func (wo *WorkersObject) workerStateStart(db *Database) {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	fmt.Println("Start worker - @StateStart")
	for {
		time.Sleep(time.Second * 5)
		startOrder := wo.GetStepOrdersWithStateStart()
		if len(startOrder) == 0 {
			// fmt.Println("@StateStart - empty")
			continue
		}
		unix := time.Now().Unix()
		for idx, value := range startOrder {
			fmt.Printf("%v driver_out after - %v | %v | %v\n", idx, value.time, time.Now().Unix(), value.lastState)
			if value.time < unix {
				wo.DelStepOrdersWithStateStart(idx)
				timingLocalCity := wo.GetTimingCity()
				wo.SetStepOrdersWithStateOnCancel(idx, time.Now().Unix()+int64(timingLocalCity[value.city].timeWaitingCheck), value.city, "start")
				dbConn.Exec(context.Background(), "SELECT pg_notify($1, $2)", "orders", fmt.Sprintf("%v*%v*%v", "time_out", idx, "start")) //action start_out
			}
		}
	}
}

func (wo *WorkersObject) workerStateDriverReady(db *Database) {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	fmt.Println("Start worker - @StateDriverReady")
	for {
		time.Sleep(time.Second * 5)
		readyDriverOrder := wo.GetStepOrdersWithStateDriverReady()
		if len(readyDriverOrder) == 0 {
			// fmt.Println("@StateDriverReady - empty")
			continue
		}
		unix := time.Now().Unix()
		for idx, value := range readyDriverOrder {
			fmt.Printf("%v client_out after - %v | %v | %v\n", idx, value.time, time.Now().Unix(), value.lastState)
			if value.time < unix {
				wo.DelStepOrdersWithStateDriverReady(idx)
				timingLocalCity := wo.GetTimingCity()
				wo.SetStepOrdersWithStateOnCancel(idx, time.Now().Unix()+int64(timingLocalCity[value.city].timeWaitingCheck), value.city, "driver_ready")
				dbConn.Exec(context.Background(), "SELECT pg_notify($1, $2)", "orders", fmt.Sprintf("%v*%v*%v", "time_out", idx, "driver_ready")) //action driver_out_time
			}
		}
	}
}

func (wo *WorkersObject) workerStateBeginTrip(db *Database) {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	fmt.Println("Start worker - @StateBeginTrip")
	for {
		time.Sleep(time.Second * 5)
		beginTripOrder := wo.GetStepOrdersWithStateBeginTrip()
		if len(beginTripOrder) == 0 {
			// fmt.Println("@StateBeginTrip - empty")
			continue
		}
		unix := time.Now().Unix()
		for idx, value := range beginTripOrder {
			fmt.Printf("%v check after - %v | %v | %v\n", idx, value.time, time.Now().Unix(), value.lastState)
			if value.time < unix {
				wo.DelStepOrdersWithStateBeginTrip(idx)
				timingLocalCity := wo.GetTimingCity()
				wo.SetStepOrdersWithStateOnCancel(idx, time.Now().Unix()+int64(timingLocalCity[value.city].timeWaitingCheck), value.city, "begin_trip")
				dbConn.Exec(context.Background(), "SELECT pg_notify($1, $2)", "orders", fmt.Sprintf("%v*%v*%v", "time_out", idx, "begin_trip")) //action check
			}
		}
	}
}

func (wo *WorkersObject) workerStateEnd(db *Database) {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	fmt.Println("Start worker - @StateEnd")
	for {
		time.Sleep(time.Second * 60)
		dbConn.Exec(context.Background(), "SELECT f_move_order()")
	}
}

func (wo *WorkersObject) workerQueueOnCancel(db *Database) {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	fmt.Println("Start worker - @QueueOnCancel")
	for {
		time.Sleep(time.Second * 7)
		cancelOrder := wo.GetStepOrdersWithStateOnCancel()
		if len(cancelOrder) == 0 {
			continue
		}
		fmt.Println("\n@OnCancel - list:")
		unix := time.Now().Unix()
		for idx, value := range cancelOrder {
			fmt.Printf("%v cancel after - %v | %v | %v\n", idx, value.time, time.Now().Unix(), value.lastState)
			if value.time < unix {
				switch value.lastState {
				case "created":
					dbConn.Exec(context.Background(), "SELECT pg_notify($1, $2)", "orders", fmt.Sprintf("%v*%v*%v", "canceled", idx, "created"))
				case "plan":
					dbConn.Exec(context.Background(), "SELECT pg_notify($1, $2)", "orders", fmt.Sprintf("%v*%v*%v", "canceled", idx, "plan"))
				case "start":
					dbConn.Exec(context.Background(), "SELECT pg_notify($1, $2)", "orders", fmt.Sprintf("%v*%v*%v", "canceled", idx, "start")) //
				case "driver_ready":
					dbConn.Exec(context.Background(), "SELECT pg_notify($1, $2)", "orders", fmt.Sprintf("%v*%v*%v", "canceled", idx, "driver_ready"))
				case "begin_trip":
					dbConn.Exec(context.Background(), "SELECT pg_notify($1, $2)", "orders", fmt.Sprintf("%v*%v*%v", "canceled", idx, "begin_trip"))
				}
				dbConn.Exec(context.Background(), "UPDATE trip_client SET status_trip = 'end', cancel = 'system' WHERE id = $1", idx)
				wo.DelStepOrdersWithStateOnCancel(idx)
			}
		}
	}
}

/*Get data from storage*/

//GetTimingCity - Get timing cities from local storeg
func (wo *WorkersObject) GetTimingCity() map[string]timeCity {
	wo.timing.Lock()
	timing := wo.timing.city
	wo.timing.Unlock()
	return timing
}

//GetStepOrdersWithStateCreated - get list orders with state @created
func (wo *WorkersObject) GetStepOrdersWithStateCreated() map[string]orderData {
	wo.orderStateCreated.Lock()
	step := wo.orderStateCreated.value
	wo.orderStateCreated.Unlock()
	return step
}

//GetStepOrdersWithStatePlan - get list orders with state @plan
func (wo *WorkersObject) GetStepOrdersWithStatePlan() map[string]orderData {
	wo.orderStatePlan.Lock()
	step := wo.orderStatePlan.value
	wo.orderStatePlan.Unlock()
	return step
}

//GetStepOrdersWithStateStart - get list orders with state @start
func (wo *WorkersObject) GetStepOrdersWithStateStart() map[string]orderData {
	wo.orderStateStart.Lock()
	step := wo.orderStateStart.value
	wo.orderStateStart.Unlock()
	return step
}

//GetStepOrdersWithStateDriverReady - get list orders with state @driverReady
func (wo *WorkersObject) GetStepOrdersWithStateDriverReady() map[string]orderData {
	wo.orderStateDriverReady.Lock()
	step := wo.orderStateDriverReady.value
	wo.orderStateDriverReady.Unlock()
	return step
}

//GetStepOrdersWithStateBeginTrip - get list orders with state @beginTrip
func (wo *WorkersObject) GetStepOrdersWithStateBeginTrip() map[string]orderData {
	wo.orderStateBeginTrip.Lock()
	step := wo.orderStateBeginTrip.value
	wo.orderStateBeginTrip.Unlock()
	return step
}

//GetStepOrdersWithStateOnCancel - get list orders with state @onCancel
func (wo *WorkersObject) GetStepOrdersWithStateOnCancel() map[string]orderData {
	wo.queueOnCancel.Lock()
	step := wo.queueOnCancel.value
	wo.queueOnCancel.Unlock()
	return step
}

//GetStepOrdersWithStateEnd - get list orders with state @end
func (wo *WorkersObject) GetStepOrdersWithStateEnd() map[string]orderData {
	wo.orderStateEnd.Lock()
	step := wo.orderStateEnd.value
	wo.orderStateEnd.Unlock()
	return step
}

/*Set data in storage*/

//SetStepOrdersWithStateCreated - get list orders with state @created
func (wo *WorkersObject) SetStepOrdersWithStateCreated(idOrder string, unix int64, city string) {
	wo.orderStateCreated.Lock()
	wo.orderStateCreated.value[idOrder] = orderData{time: unix, city: city}
	wo.orderStateCreated.Unlock()
}

//SetStepOrdersWithStatePlan - get list orders with state @plan
func (wo *WorkersObject) SetStepOrdersWithStatePlan(idOrder string, unix int64, city string) {
	wo.orderStatePlan.Lock()
	wo.orderStatePlan.value[idOrder] = orderData{time: unix, city: city}
	wo.orderStatePlan.Unlock()
}

//SetStepOrdersWithStateStart - get list orders with state @start
func (wo *WorkersObject) SetStepOrdersWithStateStart(idOrder string, unix int64, city string) {
	wo.orderStateStart.Lock()
	wo.orderStateStart.value[idOrder] = orderData{time: unix, city: city}
	wo.orderStateStart.Unlock()
}

//SetStepOrdersWithStateDriverReady - get list orders with state @driverReady
func (wo *WorkersObject) SetStepOrdersWithStateDriverReady(idOrder string, unix int64, city string) {
	wo.orderStateDriverReady.Lock()
	fmt.Println("Data add new worker - ", idOrder, unix, city)
	wo.orderStateDriverReady.value[idOrder] = orderData{time: unix, city: city}
	wo.orderStateDriverReady.Unlock()
}

//SetStepOrdersWithStateBeginTrip - get list orders with state @beginTrip
func (wo *WorkersObject) SetStepOrdersWithStateBeginTrip(idOrder string, unix int64, city string) {
	wo.orderStateBeginTrip.Lock()
	wo.orderStateBeginTrip.value[idOrder] = orderData{time: unix, city: city}
	wo.orderStateBeginTrip.Unlock()
}

//SetStepOrdersWithStateOnCancel - get list orders with state @Cancel
func (wo *WorkersObject) SetStepOrdersWithStateOnCancel(idOrder string, unix int64, city string, lastState string) {
	fmt.Println("Add in cancel - ", idOrder, " ", unix, " ", city, " ", lastState)
	wo.queueOnCancel.Lock()
	wo.queueOnCancel.value[idOrder] = orderData{time: unix, city: city, lastState: lastState}
	wo.queueOnCancel.Unlock()
}

//SetStepOrdersWithStateEnd - get list orders with state @End
func (wo *WorkersObject) SetStepOrdersWithStateEnd(idOrder string, unix int64, city string) {
	wo.orderStateEnd.Lock()
	wo.orderStateEnd.value[idOrder] = orderData{time: unix, city: city}
	wo.orderStateEnd.Unlock()
}

/*Del data in storage*/

//DelStepOrdersWithStateCreated - del object from list orders with state @created
func (wo *WorkersObject) DelStepOrdersWithStateCreated(idOrder string) {
	wo.orderStateCreated.Lock()
	delete(wo.orderStateCreated.value, idOrder)
	wo.orderStateCreated.Unlock()
}

//DelStepOrdersWithStatePlan - del object from list orders with state @plan
func (wo *WorkersObject) DelStepOrdersWithStatePlan(idOrder string) {
	wo.orderStatePlan.Lock()
	delete(wo.orderStatePlan.value, idOrder)
	wo.orderStatePlan.Unlock()
}

//DelStepOrdersWithStateStart - del object from list orders with state @start
func (wo *WorkersObject) DelStepOrdersWithStateStart(idOrder string) {
	wo.orderStateStart.Lock()
	delete(wo.orderStateStart.value, idOrder)
	wo.orderStateStart.Unlock()
}

//DelStepOrdersWithStateDriverReady - del object from list orders with state @driverReady
func (wo *WorkersObject) DelStepOrdersWithStateDriverReady(idOrder string) {
	wo.orderStateDriverReady.Lock()
	delete(wo.orderStateDriverReady.value, idOrder)
	wo.orderStateDriverReady.Unlock()
}

//DelStepOrdersWithStateBeginTrip - del object from list orders with state @beginTrip
func (wo *WorkersObject) DelStepOrdersWithStateBeginTrip(idOrder string) {
	wo.orderStateBeginTrip.Lock()
	delete(wo.orderStateBeginTrip.value, idOrder)
	wo.orderStateBeginTrip.Unlock()
}

//DelStepOrdersWithStateOnCancel - del object from list orders with state @Cancel
func (wo *WorkersObject) DelStepOrdersWithStateOnCancel(idOrder string) {
	wo.queueOnCancel.Lock()
	delete(wo.queueOnCancel.value, idOrder)
	wo.queueOnCancel.Unlock()
}

//DelInAllSatate - del object from all list
func (wo *WorkersObject) DelInAllSatate(idOrder string, cancel bool) {
	wo.DelStepOrdersWithStateCreated(idOrder)
	wo.DelStepOrdersWithStatePlan(idOrder)
	if cancel {
		fmt.Println("Clear cancel")
		wo.DelStepOrdersWithStateOnCancel(idOrder)
	}
	wo.DelStepOrdersWithStateStart(idOrder)
	wo.DelStepOrdersWithStateDriverReady(idOrder)
	wo.DelStepOrdersWithStateBeginTrip(idOrder)
}

//DelStepOrdersWithStateEnd - del object from list orders with state @End
func (wo *WorkersObject) DelStepOrdersWithStateEnd(idOrder string) {
	wo.orderStateEnd.Lock()
	delete(wo.orderStateEnd.value, idOrder)
	wo.orderStateEnd.Unlock()
}

//SetTimingCity - Get timing cities from local storeg
func (wo *WorkersObject) SetTimingCity(data map[string]timeCity) {
	wo.timing.Lock()
	wo.timing.city = data
	wo.timing.Unlock()
}
