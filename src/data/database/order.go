package database

import (
	"context"
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
	"time"

	"../../utility"
	"../model"
	"../order"
)

func convertSteptoUTC(date string, regionUTC int) (int64, error) {
	fmt.Println(date)
	var resultUnixTime, utc int64
	if regionUTC > 0 {
		utc = int64(regionUTC) * 3600
	} else {
		utc = 0
	}
	fmt.Println("UTC region - ", regionUTC, " ", utc)
	timedata := strings.Split(date, "-")
	if len(timedata) != 2 {
		timedata := strings.Split(date, " ")
		if len(timedata) != 2 {
			return 0, nil
		}
	}
	timeOrder := strings.Split(timedata[0], ":")
	dateOrder := strings.Split(timedata[1], "/")
	// fmt.Printf("Hours - %v\nMinuts - %v\nDay - %v\nMounth - %v\nYears - %v\n\n", timeOrder[0], timeOrder[1], dateOrder[0], dateOrder[1], dateOrder[2])
	if len(timeOrder) == 2 && len(dateOrder) == 3 {
		timeHours, err := strconv.Atoi(timeOrder[0])
		if err != nil {
			fmt.Println("Error convert hourse")
		}
		timeMinuts, err := strconv.Atoi(timeOrder[1])
		if err != nil {
			fmt.Println("Error convert minuts")
		}
		dateDay, err := strconv.Atoi(dateOrder[0])
		if err != nil {
			fmt.Println("Error convert days")
		}
		dateMounth, err := strconv.Atoi(dateOrder[1])
		if err != nil {
			fmt.Println("Error convert mounth")
		}
		dateYears, err := strconv.Atoi(dateOrder[2])
		if err != nil {
			fmt.Println("Error convert years")
		}
		// fmt.Printf("Hours - %v\nMinuts - %v\nDay - %v\nMounth - %v\nYears - %v\n\n", timeHours, timeMinuts, dateDay, dateMounth, dateYears)
		//resultUnixTime = ((timeMinuts * 60) + (timeHours * 3600) + (dateDay * 86400) + (dateMounth * 2678400) + (dateYears * 31556736)) - utc
		resultUnixTime = time.Date(dateYears, time.Month(dateMounth), dateDay, timeHours, timeMinuts, 0, 0, time.Local).Unix()
		return resultUnixTime, nil
	}
	return 0, nil
}

func convertUTCtoStep(unix int64, regionUTC int) (string, error) {
	var utc, dateYears, dateMounth, dateDay, timeHours, timeMinuts int64
	if regionUTC <= 0 {
		utc = int64(regionUTC) * 3600
	} else {
		utc = 0
	}
	unix += utc
	dateYears = unix / 31556736
	unix -= (31556736 * dateYears)
	dateMounth = unix / 2678400
	unix -= (2678400 * dateMounth)
	dateDay = unix / 86400
	unix -= (86400 * dateDay)
	timeHours = unix / 3600
	unix -= (3600 * timeHours)
	timeMinuts = unix / 60
	unix -= (60 * timeMinuts)
	dateYears += 1970
	dateMounth++
	dateDay++
	return fmt.Sprintf("%02d:%02d-%02d/%02d/%d\n\n", timeHours, timeMinuts, dateDay, dateMounth, dateYears), nil
}

//ConvertTimeForRegionInUnix - Convert time in usnix format
func (db *Database) ConvertTimeForRegionInUnix(region, date string) (int64, error) {
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return 0, utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	var utc int
	if err := dbConn.QueryRow(context.Background(), "SELECT utc FROM regions WHERE region = $1", region).Scan(&utc); err != nil {
		fmt.Println("UTC query error")
		utc = 0
	}
	fmt.Println("UTC+", utc)
	return convertSteptoUTC(date, utc)
}

//ConvertUnixForRegionInTimeStep - Convert unix time in time step format
func (db *Database) ConvertUnixForRegionInTimeStep(region string, unix int64) (string, error) {
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return "", utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	var utc int
	if err := dbConn.QueryRow(context.Background(), "SELECT utc FROM regions WHERE region = $1", region).Scan(&utc); err != nil {
		fmt.Println("UTC query error")
		utc = 0
	}
	return convertUTCtoStep(unix, utc)
}

//CreateOrderT - Create order in database and
func CreateOrderT(db *Database, idUser string, modelClient model.OrdermodelforCreate, localkey string) (int, model.OrderModelForSend, error) {
	var res int
	checkCreateOrder := ""
	// var err error
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	modelSend := model.OrderModelForSend{}
	defer dbConn.Release()
	dbConn.QueryRow(context.Background(), "SELECT id FROM trip_client WHERE client = $1 AND status_trip != 'end'", idUser).Scan(&checkCreateOrder)
	if len(checkCreateOrder) > 3 {
		fmt.Println("Error create order, order exist", checkCreateOrder)
		return 1, modelSend, err
	}
	//5.- Build response
	modelSend.InCity, modelSend.Places, modelSend.AddOption, err = order.DumpDataOrder(&modelClient)
	if err != nil {
		return 0, modelSend, err
	}
	if modelSend.AddOption.PlanOrder == true {
		modelSend.Status = "plan"
	} else {
		modelSend.Status = "created"
	}
	//5.1-Write data in database
	res, modelSend.ID, err = db.CreateDataOrder(idUser, &modelSend, localkey)
	if err != nil {
		return 3, modelSend, nil
	}
	if res != 0 {
		return res, modelSend, nil
	}
	return 0, modelSend, nil
}

//CreateOfferT
func CreateOfferT(db *Database, idUser string, dataOffer model.OfferDriverData) (string, error) {
	return db.CreateOffer(idUser, dataOffer)
}

//CreateRecoveryT
func CreateRecoveryT(db *Database, idUser string, idOrder string) error {
	return db.CreateRecovery(idUser, idOrder)
}

//CreateRecoveryDriverT
func CreateRecoveryDriverT(db *Database, idUser string, idOrder string) error {
	return db.CreateRecoveryDriver(idUser, idOrder)
}

//CreateRecoveryClientT
func CreateRecoveryClientT(db *Database, idUser string, idOrder string) error {
	return db.CreateRecoveryClient(idUser, idOrder)
}

//CreateRecoveryPriceUPT
func CreateRecoveryPriceUPT(db *Database, idUser string, idOrder string) error {
	return db.CreateRecoveryPriceUP(idUser, idOrder)
}

//CreateRecoveryTrueT
func CreateRecoveryTrueT(db *Database, idUser string, idOrder string) error {
	return db.CreateRecoveryTrue(idUser, idOrder)
}

//CreateRecoveryTrue
func (db *Database) CreateRecoveryTrue(idUser, idOrder string) error {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	var tripStatus string
	if err := dbConn.QueryRow(context.Background(), "SELECT status_trip FROM trip_client WHERE id = $1 AND status_trip != 'end'", idOrder).Scan(&tripStatus); err != nil {
		return err
	}
	dbConn.Exec(context.Background(), "SELECT pg_notify($1,$2)", "orders", fmt.Sprintf("%v*%v*%v*%v", "recovery", idOrder, tripStatus, "true"))
	return nil
}

//CreateRecoveryDriver
func (db *Database) CreateRecoveryDriver(idUser, idOrder string) error {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	var tripStatus string
	if err := dbConn.QueryRow(context.Background(), "SELECT status_trip FROM trip_client WHERE id = $1 AND status_trip != 'end'", idOrder).Scan(&tripStatus); err != nil {
		return err
	}
	dbConn.Exec(context.Background(), "SELECT pg_notify($1,$2)", "orders", fmt.Sprintf("%v*%v*%v*%v", "recovery", idOrder, tripStatus, "driver_true"))
	return nil
}

//CreateRecoveryClient
func (db *Database) CreateRecoveryClient(idUser, idOrder string) error {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	var tripStatus string
	if err := dbConn.QueryRow(context.Background(), "SELECT status_trip FROM trip_client WHERE id = $1 AND status_trip != 'end'", idOrder).Scan(&tripStatus); err != nil {
		return err
	}
	dbConn.Exec(context.Background(), "SELECT pg_notify($1,$2)", "orders", fmt.Sprintf("%v*%v*%v*%v", "recovery", idOrder, tripStatus, "client_true"))
	return nil
}

//CreateRecoveryPriceUP
func (db *Database) CreateRecoveryPriceUP(idUser, idOrder string) error {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	var tripStatus string
	if err := dbConn.QueryRow(context.Background(), "SELECT status_trip FROM trip_client WHERE id = $1 AND status_trip != 'end'", idOrder).Scan(&tripStatus); err != nil {
		return err
	}
	dbConn.Exec(context.Background(), "SELECT pg_notify($1,$2)", "orders", fmt.Sprintf("%v*%v*%v*%v", "recovery", idOrder, tripStatus, "price_up"))
	return nil
}

//CreateCancelT
func CreateCancelT(db *Database, idUser string, idOrder string) error {
	return db.CreateCancel(idUser, idOrder)
}

//TakeIDOrderPlan
func TakeIDOrderPlanT(db *Database, idUser string) (string, error) {
	return db.TakeIDOrderPlan(idUser)
}

//TakeIDOrder
func TakeIDOrderT(db *Database, idUser string) (string, error) {
	return db.TakeIDOrder(idUser)
}

//StartOrderT
func StartOrderT(db *Database, idUser string, startData model.StartOrderData) {
	fmt.Println(startData.Order, startData.Driver, startData.Duration)
	db.StartOrder(idUser, startData)
}

//PositionDriverT
func PositionDriverT(db *Database, idUser string, positionData model.PositionData) {
	db.PositionDriver(idUser, positionData)
	if positionData.Order != "" {
		db.PositionDriverOrder(idUser, positionData)
	}
}

//PositionClientT
func PositionClientT(db *Database, idUser string, positionData model.PositionData) {
	db.PositionClient(idUser, positionData)
}

//DriverReadyT
func DriverReadyT(db *Database, idOrder string) {
	db.DriverReady(idOrder)
}

//DriverBeginTripT
func DriverBeginTripT(db *Database, idOrder string) {
	db.DriverBeginTrip(idOrder)
}

//DriverEndTripT
func DriverEndTripT(db *Database, idOrder string) {
	db.DriverEndTrip(idOrder)
}

//MessageDialogT
func MessageDialogT(db *Database, idOrder string, messageData model.DataOrder) {
	db.MessageDialog(idOrder, messageData)
}

//ReplayCreateOrderT
func ReplayCreateOrderT(db *Database, idOrder, idUser string, price int, localkey string) error {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	optionData := model.OptionData{}
	var inCity bool
	var idNewOrder string
	var places []byte
	var dateStart int64
	if err := dbConn.QueryRow(context.Background(),
		"SELECT places, in_city, plane, date_plane, date_start, babych, smoking, fastup, animal, comment, pay FROM trip_client WHERE id = $1",
		idOrder).Scan(&places, &inCity, &optionData.PlanOrder, &optionData.PlanTime, &dateStart, &optionData.Babych, &optionData.Smoking, &optionData.FastUp, &optionData.Animal, &optionData.Comment, &optionData.SystemPay); err != nil {
		fmt.Println("Take data - ", err.Error())
		return err
	}
	dbConn.Exec(context.Background(), "UPDATE trip_client SET status_trip = 'end' WHERE id = $1", idOrder)
	if err := dbConn.QueryRow(context.Background(),
		"SELECT f_new_trip($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15)",
		idUser,
		places,
		inCity,
		optionData.PlanOrder,
		0,
		"created",
		dateStart,
		price,
		optionData.Babych,
		optionData.Smoking,
		optionData.FastUp,
		optionData.Animal,
		optionData.Comment,
		optionData.SystemPay,
		localkey).Scan(&idNewOrder); err != nil {
		fmt.Println("f_new_trip - ", err.Error())
		return err
	}
	db.ReplayCreateOrder(idOrder, idNewOrder, idUser, localkey)
	return nil
}

//ReplayCreateOrder
func (db *Database) ReplayCreateOrder(idOrder, idNewOrder, idUser, localkey string) {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	dbConn.Exec(context.Background(), "SELECT pg_notify($1,$2)", "orders",
		fmt.Sprintf("%v*%v*%v*%v", "replay", idOrder, idNewOrder, idUser))
}

//DriverReady
func (db *Database) DriverBeginTrip(idOrder string) {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	dbConn.Exec(context.Background(), "SELECT pg_notify($1,$2)", "orders",
		fmt.Sprintf("%v*%v", "begin_trip", idOrder))
	dbConn.Exec(context.Background(), "UPDATE trip_client SET status_trip = 'begin_trip' WHERE id = $1", idOrder)
}

//DriverEndTrip
func (db *Database) DriverEndTrip(idOrder string) {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	dbConn.Exec(context.Background(), "SELECT pg_notify($1,$2)", "orders",
		fmt.Sprintf("%v*%v", "end", idOrder))
	dbConn.Exec(context.Background(), "UPDATE trip_client SET status_trip = 'end' WHERE id = $1", idOrder)
}

//DriverReady
func (db *Database) DriverReady(idOrder string) {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	dbConn.Exec(context.Background(), "SELECT pg_notify($1,$2)", "orders",
		fmt.Sprintf("%v*%v", "driver_ready", idOrder))
	dbConn.Exec(context.Background(), "UPDATE trip_client SET status_trip = 'driver_ready' WHERE id = $1", idOrder)
}

//PositionDriverOrder
func (db *Database) PositionDriverOrder(idUser string, positionData model.PositionData) {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if positionData.Lat != 0 && positionData.Lon != 0 {
		dbConn.Exec(context.Background(), "SELECT pg_notify($1,$2)", "orders",
			fmt.Sprintf("%v*%v*%v*%v", "position_driver", positionData.Order, positionData.Lat, positionData.Lon))
	}
}

//PositionDriver
func (db *Database) PositionDriver(idUser string, positionData model.PositionData) {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if positionData.Lat != 0 && positionData.Lon != 0 {
		dbConn.Exec(context.Background(), "SELECT pg_notify($1,$2)", "position",
			fmt.Sprintf("%v*%v*%v", idUser, positionData.Lat, positionData.Lon))
	}
}

//PositionClient
func (db *Database) PositionClient(idUser string, positionData model.PositionData) {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if positionData.Lat != 0 && positionData.Lon != 0 {
		dbConn.Exec(context.Background(), "SELECT pg_notify($1,$2)", "orders",
			fmt.Sprintf("%v*%v*%v*%v", "position_client", positionData.Order, positionData.Lat, positionData.Lon))
	}
}

//MessageDialog
func (db *Database) MessageDialog(idUser string, messageData model.DataOrder) {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	dbConn.Exec(context.Background(), "SELECT pg_notify($1,$2)", "orders",
		fmt.Sprintf("%v*%v*%v*%v", "message", messageData.Order, idUser, messageData.Value))
}

//StartOrder
func (db *Database) StartOrder(idUser string, startData model.StartOrderData) {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	var idDriver string
	if err := dbConn.QueryRow(context.Background(), "UPDATE trip_client SET driver = $1, status_trip = 'start' WHERE id = $2 RETURNING driver", startData.Driver, startData.Order).Scan(&idDriver); err != nil {
		fmt.Println("UPDATE ", err.Error())
		return
	}
	fmt.Println(idDriver, startData.Driver)
	if idDriver == startData.Driver {
		dbConn.Exec(context.Background(), "SELECT pg_notify($1,$2)", "orders",
			fmt.Sprintf("%v*%v*%v", "start", startData.Order, startData.Duration))
	}
	return
}

//TakeIDOrderPlan
func (db *Database) TakeIDOrderPlan(idUser string) (string, error) {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	var idOrder string
	if err := dbConn.QueryRow(context.Background(), "SELECT id FROM trip_client WHERE (client = $1 OR driver = $1) AND (status_trip == 'plan' AND status_trip = 'plan_add_driver')",
		idUser).Scan(&idOrder); err != nil {
		return "", err
	}
	return idOrder, nil
}

//TakeIDOrder
func (db *Database) TakeIDOrder(idUser string) (string, error) {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	var idOrder string
	if err := dbConn.QueryRow(context.Background(), "SELECT id FROM trip_client WHERE (client = $1 OR driver = $1) AND (status_trip != 'plan' AND status_trip != 'plan_add_driver' AND status_trip != 'end')",
		idUser).Scan(&idOrder); err != nil {
		return "", err
	}
	return idOrder, nil
}

//CreateOffer -
func (db *Database) CreateOffer(idUser string, dataOffer model.OfferDriverData) (string, error) {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	var idCreator string
	var cex_self, cex_arend, ex_session bool
	if err := dbConn.QueryRow(context.Background(),
		"SELECT CASE WHEN ((cr.car = NULL) OR (LENGTH(cr.car) = 0)) THEN FALSE ELSE TRUE END AS self, CASE WHEN ((cr.car_arend = NULL) OR (LENGTH(cr.car_arend) = 0)) THEN FALSE ELSE TRUE END AS arend FROM (SELECT car, car_arend FROM drivers WHERE id = $1) AS cr",
		idUser).Scan(&cex_self, &cex_arend); err != nil {
		return "", err
	}
	if !cex_arend {
		if !cex_self {
			return "invalid", nil
		} else {
			if err := dbConn.QueryRow(context.Background(),
				"SELECT CASE WHEN (status_pay='SUCCEEDED') THEN TRUE ELSE FALSE END AS res FROM transaction_dr WHERE driver = $1 ORDER BY date_tr DESC LIMIT 1",
				idUser).Scan(&ex_session); err != nil {
				return "invalid", err
			}
			if !ex_session {
				return "invalid", nil
			}
		}
	}
	if err := dbConn.QueryRow(context.Background(), "SELECT client FROM trip_client WHERE id = $1", dataOffer.IDOrder).Scan(&idCreator); err != nil {
		return "", err
	}
	dbConn.Exec(context.Background(), "SELECT pg_notify($1,$2)", "orders", fmt.Sprintf("%v*%v*%v*%v*%v*%v",
		"offer", idUser, dataOffer.Duration, dataOffer.Lat, dataOffer.Lon, idCreator))
	return "", nil
}

//CreateRecovery
func (db *Database) CreateRecovery(idUser, idOrder string) error {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	var tripStatus string
	if err := dbConn.QueryRow(context.Background(), "SELECT status_trip FROM trip_client WHERE id = $1 AND status_trip != 'end'", idOrder).Scan(&tripStatus); err != nil {
		return err
	}
	dbConn.Exec(context.Background(), "SELECT pg_notify($1,$2)", "orders", fmt.Sprintf("%v*%v*%v*%v", "recovery", idOrder, tripStatus, "true"))
	return nil
}

//CreateCancel
func (db *Database) CreateCancel(idUser, idOrder string) error {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	var tripStatus, object string
	if err := dbConn.QueryRow(context.Background(), "SELECT status_trip, CASE WHEN client = $2 THEN 'client' WHEN driver = $2 THEN 'driver' ELSE 'system' END AS user FROM trip_client WHERE id = $1 AND status_trip != 'end'", idOrder, idUser).Scan(&tripStatus, &object); err != nil {
		return err
	}
	dbConn.Exec(context.Background(), "SELECT pg_notify($1,$2)", "orders", fmt.Sprintf("%v*%v*%v", "canceled", idOrder, tripStatus))
	dbConn.QueryRow(context.Background(), "UPDATE trip_client SET status_trip = 'end', cancel = $2 WHERE id = $1", idOrder, object)
	return nil
}

//checkExistsOrder
func (db *Database) checkExistsOrder(idUser string) bool {
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return true
	}
	limitTime := time.Now().Unix() + (20 * 60)
	var id string
	if err := dbConn.QueryRow(context.Background(), "SELECT id FROM (SELECT id, status_trip, date_plane FROM trip_client WHERE status_trip != 'end' AND status_trip != 'end_trip' AND client = $1) AS trip WHERE trip.status_trip != 'plan' OR ((status_trip = 'plan' OR trip.status_trip = 'plan_add_driver' OR trip.status_trip = 'plan_del_driver') AND date_plane < $2)", idUser, limitTime).Scan(&id); err != nil {
		fmt.Println("Exist order")
		return false
	}
	return true
}

//checkExistsPlanOrder
func (db *Database) checkExistsPlanOrder(idUser string) bool {
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return true
	}
	var id string
	if err := dbConn.QueryRow(context.Background(), "SELECT id FROM (SELECT id, status_trip, date_plane FROM trip_client WHERE status_trip != 'end' OR status_trip != 'end_trip' AND client = $1) AS trip WHERE status_trip = 'plan' OR trip.status_trip = 'plan_add_driver' OR trip.status_trip = 'plan_del_driver'", idUser).Scan(&id); err != nil {
		fmt.Println(err.Error())
		fmt.Println("Existst plan")
		return false
	}
	return true
}

//CreateDataOrder - create order in database
func (db *Database) CreateDataOrder(idUser string, dataOrderForSend *model.OrderModelForSend, localkey string) (int, string, error) {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	// defer dbConn.Release()
	var idOrder string
	dataPlaces, err := json.Marshal(dataOrderForSend.Places)
	timeStart := time.Now().Unix()
	citySplit := strings.Split(dataOrderForSend.Places[0].City, " ")
	timePlane, err := db.ConvertTimeForRegionInUnix(citySplit[0], dataOrderForSend.AddOption.PlanTime)
	if err != nil {
		dbConn.Release()
		return 0, "", utility.ErrorHandler(errorPQX+": CreateDataOrder.ConvertTimeForRegionInUnix ", err)
	}
	if dataOrderForSend.AddOption.PlanOrder != true {
		//check exists orders and plane order (20 min)
		check := db.checkExistsOrder(idUser)
		if check == true {
			dbConn.Release()
			return 2, "", nil
		}
	} else {
		//check exists plane order
		check := db.checkExistsPlanOrder(idUser)
		if check == true {
			dbConn.Release()
			return 2, "", nil
		}
	}
	//1.- id клиента, 2.- JSONb данные заказа, 3.- в городе, 4.- запланированный, 5.- время планирования
	//6.- статус заказа, 7.- время начала заказа, 8.- детское кресло, 9.- курить в салоне, 10.- быстрая подача,
	//11.- животные, 12.- комментарий, 13.- содержимое комментария, 14.- тип оплаты
	if err = dbConn.QueryRow(context.Background(),
		"SELECT f_new_trip($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15)",
		idUser,
		dataPlaces,
		dataOrderForSend.InCity,
		dataOrderForSend.AddOption.PlanOrder,
		timePlane,
		dataOrderForSend.Status,
		timeStart,
		dataOrderForSend.AddOption.Price,
		dataOrderForSend.AddOption.Babych,
		dataOrderForSend.AddOption.Smoking,
		dataOrderForSend.AddOption.FastUp,
		dataOrderForSend.AddOption.Animal,
		dataOrderForSend.AddOption.Comment,
		dataOrderForSend.AddOption.SystemPay,
		localkey).Scan(&idOrder); err != nil {
		fmt.Println(err.Error())
		dbConn.Release()
		return 0, "", err
	}
	dbConn.Release()
	//notify create order
	dbConn, _ = db.dbConnectPool.Acquire(context.Background())
	fmt.Printf("\n\nAction - create order. Action on %v\n\n", localkey)
	dbConn.Exec(context.Background(), "SELECT pg_notify($1,$2)", localkey, fmt.Sprintf("%v*%v", "created", idOrder))
	dbConn.Release()
	dbConn, _ = db.dbConnectPool.Acquire(context.Background())
	dbConn.Exec(context.Background(), "SELECT pg_notify($1,$2)", "service", "short_call")
	dbConn.Release()
	return 0, idOrder, nil
}

//LoadOrderData - add
func (db *Database) LoadOrderData() error {
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	return nil
}

//CancelOrder - mark order on delete in database (don't drop row)
func (db *Database) CancelOrder() error {
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	return nil
}

//KeepOrder - keep order to driver
func (db *Database) KeepOrder() error {
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	return nil
}

//ReadyDriverInOrder - mark order on (driver ready)
func (db *Database) ReadyDriverInOrder() error {
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	return nil
}

//ReadyUserInOrder - mark order on (start)
func (db *Database) ReadyUserInOrder() error {
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	return nil
}

//ReadyOrder - mark order on ready (success end of order)
func (db *Database) ReadyOrder() error {
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	return nil
}

//SetRatingUsers
func (db *Database) SetRatingUser(idUser string, rating bool) error {
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return err
	}
	var idOrder string
	var pers bool
	var client, driver string
	time.Sleep(time.Second * 20)
	if err := dbConn.QueryRow(context.Background(), "SELECT id, CASE WHEN (client = $1) THEN TRUE ELSE FALSE END as pers, COALESCE(client, 'no') AS cl, COALESCE(driver, 'no') AS dr FROM orders WHERE client = $1 OR driver = $1 ORDER BY date_start DESC LIMIT 1",
		idUser).Scan(&idOrder, &pers, &client, &driver); err != nil {
		return err
	}
	if pers {
		dbConn.Exec(context.Background(), "UPDATE orders SET rating_client = $1 WHERE id = $2", rating, idOrder)
		if driver != "no" {
			time.Sleep(time.Second * 1)
			dbConn.Release()
			dbConn, err = db.dbConnectPool.Acquire(context.Background())
			if err != nil {
				return err
			}
			dbConn.Exec(context.Background(), "SELECT f_rating($1,$2)", driver, rating)
			defer dbConn.Release()
		}
	} else {
		dbConn.Exec(context.Background(), "UPDATE orders SET rating_driver = $1 WHERE id = $2", rating, idOrder)
		if client != "no" {
			time.Sleep(time.Second * 1)
			dbConn.Release()
			dbConn, err = db.dbConnectPool.Acquire(context.Background())
			if err != nil {
				return err
			}
			dbConn.Exec(context.Background(), "SELECT f_rating($1,$2)", client, rating)
			defer dbConn.Release()
		}
	}
	return nil
}
