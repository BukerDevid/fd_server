package utility

//Configuration struct needed for mapping config file in object.
type Configuration struct {
	ConnectStrPg   string `json:"conne_db_pg"`
	AddressServ    string `json:"address_server"`
	Cert           string `json:"certFile"`
	Key            string `json:"keyFile"`
	ScarAddrServer string `json:"scarAddr"`
	ScarCrt        string `json:"scarCrt"`
	ScarKey        string `json:"scarKey"`
	ScarID         string `json:"scarId"`
}

//IsValid checking fields for emptitie
func (obj *Configuration) IsValid() bool {
	if obj != nil {
		return (obj.AddressServ != "" && obj.ConnectStrPg != "") && (obj.Cert != "" && obj.Key != "") && (obj.ScarAddrServer != "" && obj.ScarCrt != "") && (obj.ScarKey != "" && obj.ScarID != "")
	}
	return false
}
