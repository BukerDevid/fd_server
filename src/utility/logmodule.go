package utility

import (
	"os"
	"time"
)

var (
	errorLog = "Log module error# "
	logFile  *os.File
)

//TODO add log in rsyslog
/*
	programName := filepath.Base(os.Args[0])
	sysLog, err := syslog.New(syslog.LOG_INFO|syslog.LOG_LOCAL6, programName)
	if err != nil {
		log.Fatal(err)
	} else {
		log.SetOutput(sysLog)
	}
	log.Println("LOG_INFO + LOG_LOCAL6: Start GO test")
*/

//CreateLog - fuction for logs file
func CreateLog(path string) error {
	file, errCreate := os.OpenFile(path, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if errCreate != nil {
		return ErrorHandler(errorLog, errCreate)
	}
	logFile = file
	return nil
}

//WriteLog - fuction for write log into log file
func WriteLog(str string) error {
	_, err := logFile.WriteString(time.Now().String() + " " + str + "\n")
	if err != nil {
		return err
	}
	return nil
}
