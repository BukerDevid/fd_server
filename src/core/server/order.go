package server

import (
	"encoding/json"
	"fmt"
	"sync"
	"time"

	"../../data/database"
	"../../data/model"
	"github.com/gorilla/websocket"
)

//WebConn - list websocket connection
type WebConn struct {
	sync.Mutex
	connect map[string]WebSocketConnect
}

//WebDriver - list websocket connection driver
type WebDriver struct {
	sync.Mutex
	connect map[string]WebSocketConnect
}

//WebSocketConnect - struct connection
type WebSocketConnect struct {
	Addr   string
	WSConn *websocket.Conn
	Status chan string
	City   string
}

//SocketJSON - object for list
type SocketJSON struct {
	// idx    int    `json:"index"`
	IDUser string `json:"user"`
	Addr   string `json:"addr"`
}

//SocketJSONDriver - driver
type SocketJSONDriver struct {
	// idx    int    `json:"index"`
	IDUser string `json:"user"`
	Addr   string `json:"addr"`
	City   string `json:"city"`
}

//GetWebConnects - Get list connects in real time
func (wo *WebConn) GetWebConnects() map[string]WebSocketConnect {
	wo.Lock()
	data := wo.connect
	wo.Unlock()
	return data
}

//CheckWebConnects - Get list connects in real time
func (wo *WebConn) CheckWebConnects(conn *websocket.Conn) {
	connects := wo.GetWebConnects()
	for idx, data := range connects {
		if data.WSConn == conn {
			wo.DelWebConnect(idx)
			fmt.Println("socket: pop map")
			return
		}
	}
}

//CheckAllConnects - Get list connects in real time
func (wo *WebConn) CheckAllConnects() {
	for {
		time.Sleep(time.Second * 10)
		connects := wo.GetWebConnects()
		for _, data := range connects {
			if err := data.WSConn.WriteMessage(1, nil); err != nil {
				fmt.Println("socket: kill die connect!", data.WSConn.RemoteAddr())
				data.WSConn.Close()
			}
		}
	}
}

//DelWebConnect - Delete connect from ist connect
func (wo *WebConn) DelWebConnect(idUser string) {
	wo.Lock()
	delete(wo.connect, idUser)
	wo.Unlock()
}

//SetWebConnects - Set connect in list
func (wo *WebConn) SetWebConnects(id string, connect WebSocketConnect) {
	wo.Lock()
	wo.connect[id] = connect
	wo.Unlock()
}

//GetWebConnectsDriver - Get list connects in real time
func (wo *WebDriver) GetWebConnectsDriver() map[string]WebSocketConnect {
	wo.Lock()
	data := wo.connect
	wo.Unlock()
	return data
}

//DelWebConnectDriver - Get list connects in real time
func (wo *WebDriver) DelWebConnectDriver(idUser string) {
	wo.Lock()
	delete(wo.connect, idUser)
	wo.Unlock()
}

//SetWebConnectsDriver - Set connect in list
func (wo *WebDriver) SetWebConnectsDriver(id string, connect WebSocketConnect) {
	wo.Lock()
	wo.connect[id] = connect
	wo.Unlock()
}

//BroadcastSendOnCity - Send message for all driver in city (Connected driver)
func (wo *WebDriver) BroadcastSendOnCity(idCity string, data []byte) {
	list := wo.GetWebConnectsDriver()
	for _, connect := range list {
		if connect.City == idCity {
			connect.WSConn.WriteMessage(1, data)
		}
	}
}

//SendUser -
func (wo *WebConn) SendUser(data []byte) {
	var modelWorkModelEvent model.WorkModelEvent
	json.Unmarshal(data, &modelWorkModelEvent)
	list := wo.GetWebConnects()
	if sock, ok := list[modelWorkModelEvent.Data.User]; ok {
		if err := sock.WSConn.WriteMessage(1, data); err != nil {
			fmt.Println("Error write to socket - ", err.Error())
		}
	}
}

// func (wo *WebConn) SendUserRecovery(data []byte) {
// 	var modelWorkModelEventTimeOutOrder model.WorkModelEventTimeOutOrder
// 	json.Unmarshal(data, &modelWorkModelEventTimeOutOrder)
// 	list := wo.GetWebConnects()
// 	list[modelWorkModelEventTimeOutOrder.Data.User].WSConn.WriteMessage(1, data)
// }

//SendUserOffer
func (wo *WebConn) SendUserOffer(data []byte) {
	var modelWorkModelDriverOrderDataOffer model.WorkModelDriverOrderDataOffer
	json.Unmarshal(data, &modelWorkModelDriverOrderDataOffer)
	list := wo.GetWebConnects()
	if sock, ok := list[modelWorkModelDriverOrderDataOffer.Data.IDUser]; ok {
		if err := sock.WSConn.WriteMessage(1, data); err != nil {
			fmt.Println("Error write to socket - ", err.Error())
		} else {
			fmt.Println("Offer for user - ", modelWorkModelDriverOrderDataOffer.Data.IDUser)
		}
	}
}

//ListWocketsUP - List up websocket
func (wo *WebConn) ListWocketsUP() ([]byte, error) {
	list := wo.GetWebConnects()
	jsonData := []SocketJSON{}
	for idx, connect := range list {
		fmt.Printf("%v %v", idx, connect.Addr)
		elementList := SocketJSON{}
		elementList.IDUser = idx
		elementList.Addr = connect.Addr
		jsonData = append(jsonData, elementList)
	}
	return json.Marshal(jsonData)
}

//ListWocketsUP - List up websocket
func (wo *WebDriver) ListWocketsUP() ([]byte, error) {
	list := wo.GetWebConnectsDriver()
	jsonData := []SocketJSONDriver{}
	for idx, connect := range list {
		fmt.Printf("%v %v", idx, connect.Addr)
		elementList := SocketJSONDriver{}
		elementList.IDUser = idx
		elementList.Addr = connect.Addr
		elementList.City = connect.City
		jsonData = append(jsonData, elementList)
	}
	return json.Marshal(jsonData)
}

//WSReaderClient - for reading data form WebSocket
func (ws *WebSocketConnect) WSReaderClient(idUser string, db *database.Database, wc *WebConn, wd *WebDriver, localkey string) {
	for {
		select {
		case n := <-ws.Status:
			if n == "Close" {
				fmt.Println("socket: close reader")
				ws.WSConn.Close()
				wc.DelWebConnect(idUser)
				return
			}
		default:
			fmt.Println("Data in buf")
			if _, data, err := ws.WSConn.ReadMessage(); err == nil {
				go ws.EventList(idUser, data, db, wc, wd, localkey)
			} else {
				fmt.Println("socket: close - ", ws.Addr)
				wc.DelWebConnect(idUser)
				ws.Status <- "Close"
			}
		}
	}
}

//WSWriterClient - for Writing data from out Linsting bus in WebSocket
func (ws *WebSocketConnect) WSWriterClient( /*conn *websocket.Conn*/ msg []byte) {
	ws.WSConn.WriteMessage(1, msg)
}

//SendStart -
func (wo *WebConn) SendStart(idUser string, data []byte) {
	fmt.Println("idUser", idUser)
	list := wo.GetWebConnects()
	if SOCKET, ok := list[idUser]; ok {
		if err := SOCKET.WSConn.WriteMessage(1, data); err != nil {
			fmt.Println("Error write to socket - ", err.Error())
		}
	}
}

//SendPosition - Send position client or driver in trip
func (wo *WebConn) SendPosition(data []byte) {
	var jsonData model.PositionEvent
	err := json.Unmarshal(data, &jsonData)
	if err != nil {
		return
	}
	list := wo.GetWebConnects()
	if SOCKET, ok := list[jsonData.Data.User]; ok {
		if err := SOCKET.WSConn.WriteMessage(1, data); err != nil {
			fmt.Println("Error write to socket - ", err.Error())
		}
	}
}

//EventList - List user event
func (ws *WebSocketConnect) EventList(idUser string, data []byte, db *database.Database, wc *WebConn, wd *WebDriver, localkey string) {
	// var umDataClient interface{}
	jsonDataClient := model.WorkModelRead{}
	err := json.Unmarshal(data, &jsonDataClient)
	if err == nil {
		fmt.Println("Read event ", jsonDataClient.TypeAction)
		switch jsonDataClient.TypeAction {
		case "create":
			{
				DataCreateOrder, ok := parseOrderFromInterfaceInJSONb(jsonDataClient.Value)
				if ok != true {
					data, _ := json.Marshal(model.ActionOnly{Type: "create", Object: "error"})
					ws.WSWriterClient(data)
					return
				}
				check, _, err := database.CreateOrderT(db, idUser, DataCreateOrder, localkey)
				if err != nil {
					data, _ := json.Marshal(model.ActionOnly{Type: "create", Object: "error"})
					ws.WSWriterClient(data)
					return
				}
				if check == 1 {
					data, _ := json.Marshal(model.ActionOnly{Type: "create", Object: "error"})
					ws.WSWriterClient(data)
					return
				}
				data, _ := json.Marshal(model.ActionOnly{Type: "create", Object: "success"})
				ws.WSWriterClient(data)
				return
			}
		case "offer":
			{
				DataOffer, ok := parseOfferDriverInJSONb(jsonDataClient.Value)
				if ok != true {
					fmt.Println("parse error")
					data, _ := json.Marshal(model.ActionOnly{Type: "offer", Object: "error"})
					ws.WSWriterClient(data)
					return
				}
				res, err := database.CreateOfferT(db, idUser, DataOffer)
				if res == "invalid" {
					data, _ := json.Marshal(model.ActionOnly{Type: "offer", Object: "invalid"})
					ws.WSWriterClient(data)
					return
				}
				if err != nil {
					fmt.Println("create offer error")
					data, _ := json.Marshal(model.ActionOnly{Type: "offer", Object: "error"})
					ws.WSWriterClient(data)
					return
				}
				data, _ := json.Marshal(model.ActionOnly{Type: "offer", Object: "success"})
				ws.WSWriterClient(data)
				return
			}
		case "recovery":
			{
				idOrder, err := database.TakeIDOrderT(db, idUser)
				if err != nil {
					return
				}
				var replay model.RecoveryAction
				json.Unmarshal(data, &replay)
				fmt.Println("Recovery type: ", replay.Data.Comment)
				switch replay.Data.Comment {
				case "driver_true":
					fmt.Println("Driver true")
					database.CreateRecoveryDriverT(db, idUser, idOrder)
				case "client_true":
					fmt.Println("Client true")
					database.CreateRecoveryClientT(db, idUser, idOrder)
				case "price_up":
					fmt.Println("Price up")
					database.CreateRecoveryPriceUPT(db, idUser, idOrder)
				case "true":
					fmt.Println("Price up")
					database.CreateRecoveryTrueT(db, idUser, idOrder)
				default:
					database.CreateRecoveryT(db, idUser, idOrder)
					return
				}
			}
		case "cancel":
			{
				var idOrder string
				var err error
				if jsonDataClient.Value.(string) == "plan" {
					idOrder, err = database.TakeIDOrderPlanT(db, idUser)
				} else {
					idOrder, err = database.TakeIDOrderT(db, idUser)
				}
				if err != nil {
					return
				}
				database.CreateCancelT(db, idUser, idOrder)
				if err != nil {
					return
				}
				return
			}
		case "start":
			{
				DataDriver, ok := parseDriverDataFromStartInJSONb(jsonDataClient.Value)
				if ok != true {
					fmt.Println("parse error")
					return
				}
				DataDriver.Order, err = database.TakeIDOrderT(db, idUser)
				database.StartOrderT(db, idUser, DataDriver)
				return
			}
		case "position_driver":
			{
				DataPosition, ok := parsePositionJSONb(jsonDataClient.Value)
				if ok != true {
					fmt.Println("parse error")
					return
				}
				database.PositionDriverT(db, idUser, DataPosition)
			}
		case "position_client":
			{
				DataPosition, ok := parsePositionJSONb(jsonDataClient.Value)
				if ok != true {
					fmt.Println("parse error")
					return
				}
				database.PositionClientT(db, idUser, DataPosition)
			}
		case "message":
			{
				DataMessage, ok := parseMessageJSONb(jsonDataClient.Value)
				if ok != true {
					fmt.Println("parse error")
					return
				}
				database.MessageDialogT(db, idUser, DataMessage)
			}
		case "driver_ready":
			{
				var idOrder string
				idOrder, err = database.TakeIDOrderT(db, idUser)
				if err != nil {
					return
				}
				database.DriverReadyT(db, idOrder)
			}
		case "begin_trip":
			{
				var idOrder string
				idOrder, err = database.TakeIDOrderT(db, idUser)
				if err != nil {
					return
				}
				database.DriverBeginTripT(db, idOrder)
			}
		case "replay":
			{
				idOrder, err := database.TakeIDOrderT(db, idUser)
				if err != nil {
					data, _ := json.Marshal(model.ActionOnly{Type: "replay", Object: "error"})
					ws.WSWriterClient(data)
					return
				}
				var replay model.RecoveryAction
				json.Unmarshal(data, &replay)
				fmt.Println("Price - ", replay.Data.Price)
				if replay.Data.Price != 0 {
					database.ReplayCreateOrderT(db, idOrder, idUser, replay.Data.Price, localkey)
					data, _ := json.Marshal(model.ActionOnly{Type: "replay", Object: "success"})
					ws.WSWriterClient(data)
					return
				}
				data, _ := json.Marshal(model.ActionOnly{Type: "replay", Object: "error"})
				ws.WSWriterClient(data)
			}
		case "end":
			{
				var idOrder string
				idOrder, err = database.TakeIDOrderT(db, idUser)
				if err != nil {
					return
				}
				database.DriverEndTripT(db, idOrder)
			}
		case "ping":
			{
				ws.WSWriterClient([]byte("fucking - pong"))
			}
		}
	} else {
		data, _ := json.Marshal(model.WorkModelSend{Type: "data", Object: "error"})
		ws.WSWriterClient(data)
	}
}

func parseOrderFromInterfaceInJSONb(data interface{}) (model.OrdermodelforCreate, bool) {
	jsonModel := model.OrdermodelforCreate{}
	jsonBinary, err := json.Marshal(data)
	if err != nil {
		fmt.Println("False parse")
		return jsonModel, false
	}
	err = json.Unmarshal(jsonBinary, &jsonModel)
	if err != nil {
		fmt.Println("False parse")
		return jsonModel, false
	}
	return jsonModel, true
}

func parseOrderFromClientStartInJSONb(data interface{}) (model.ReadyOrderModelClient, bool) {
	jsonModel := model.ReadyOrderModelClient{}
	jsonBinary, err := json.Marshal(data)
	if err != nil {
		fmt.Println("False parse")
		return jsonModel, false
	}
	err = json.Unmarshal(jsonBinary, &jsonModel)
	if err != nil {
		fmt.Println("False parse")
		return jsonModel, false
	}
	return jsonModel, true
}

func parseOrderFromDriverStartInJSONb(data interface{}) (model.ReadyOrderModelDriver, bool) {
	jsonModel := model.ReadyOrderModelDriver{}
	jsonBinary, err := json.Marshal(data)
	if err != nil {
		fmt.Println("False parse")
		return jsonModel, false
	}
	err = json.Unmarshal(jsonBinary, &jsonModel)
	if err != nil {
		fmt.Println("False parse")
		return jsonModel, false
	}
	return jsonModel, true
}

//parseDriverDataFromStartInJSONb - write JSON
func parseDriverDataFromStartInJSONb(data interface{}) (model.StartOrderData, bool) {
	jsonModel := model.StartOrderData{}
	jsonBinary, err := json.Marshal(data)
	if err != nil {
		fmt.Println("False parse")
		return jsonModel, false
	}
	err = json.Unmarshal(jsonBinary, &jsonModel)
	if err != nil {
		fmt.Println("False parse")
		return jsonModel, false
	}
	return jsonModel, true
}

func parseMessageJSONb(data interface{}) (model.DataOrder, bool) {
	var jsonModel model.DataOrder
	jsonBinary, err := json.Marshal(data)
	if err != nil {
		fmt.Println("False parse")
		return jsonModel, false
	}
	err = json.Unmarshal(jsonBinary, &jsonModel)
	if err != nil {
		fmt.Println("False parse")
		return jsonModel, false
	}
	return jsonModel, true
}

func parsePositionJSONb(data interface{}) (model.PositionData, bool) {
	var modelPositionData model.PositionData
	jsonBinary, err := json.Marshal(data)
	if err != nil {
		fmt.Println("False parse")
		return modelPositionData, false
	}
	err = json.Unmarshal(jsonBinary, &modelPositionData)
	if err != nil {
		fmt.Println("False parse")
		return modelPositionData, false
	}
	return modelPositionData, true
}

func parseCreatedOrderFromInterfaceInJSONb(data interface{}) (model.ActionCreateOrderData, bool) {
	var modelActionCreateOrderData model.ActionCreateOrderData
	jsonBinary, err := json.Marshal(data)
	if err != nil {
		fmt.Println("False parse")
		return modelActionCreateOrderData, false
	}
	err = json.Unmarshal(jsonBinary, &modelActionCreateOrderData)
	if err != nil {
		fmt.Println("False parse")
		return modelActionCreateOrderData, false
	}
	return modelActionCreateOrderData, true
}

func parseOfferDriverInJSONb(data interface{}) (model.OfferDriverData, bool) {
	var modelOfferDriverData model.OfferDriverData
	jsonBinary, err := json.Marshal(data)
	if err != nil {
		fmt.Println("False parse")
		return modelOfferDriverData, false
	}
	err = json.Unmarshal(jsonBinary, &modelOfferDriverData)
	if err != nil {
		fmt.Println("False parse")
		return modelOfferDriverData, false
	}
	return modelOfferDriverData, true
}

func parseDriverDataFromOfferInJSONb(data interface{}) (model.StartOrderData, bool) {
	var modelStartOrderData model.StartOrderData
	jsonBinary, err := json.Marshal(data)
	if err != nil {
		fmt.Println("False parse")
		return modelStartOrderData, false
	}
	err = json.Unmarshal(jsonBinary, &modelStartOrderData)
	if err != nil {
		fmt.Println("False parse")
		return modelStartOrderData, false
	}
	return modelStartOrderData, true
}

//ActionFromDatabase - bus action from database
func (root *Core) ActionFromDatabase(act chan interface{}) {
	fmt.Println("Keep channel action!")
	for {
		select {
		case data := <-act:
			jsonData := model.WorkModelSend{}
			if err := json.Unmarshal(data.([]byte), &jsonData); err == nil {
				switch jsonData.Object {
				case "created":
					createJSONData := model.ActionCreateOrderData{}
					if err := json.Unmarshal(data.([]byte), &createJSONData); err == nil {
						idCity, err := database.TakeCityIDOnNameCity(root.db, createJSONData.Data.Places[0].City)
						if err == nil {
							fmt.Println("Write driver create order!")
							root.wd.BroadcastSendOnCity(idCity, data.([]byte))
						}
					}
				case "start":
					if driverJSON, ok := parseOrderFromDriverStartInJSONb(jsonData.Data); ok {
						root.wc.SendStart(driverJSON.ID, data.([]byte))
					} else if clientJSON, ok := parseOrderFromDriverStartInJSONb(jsonData.Data); ok {
						root.wc.SendStart(clientJSON.ID, data.([]byte))
					}
				case "position":
					{
						root.wc.SendPosition(data.([]byte))
					}
				case "time_out":
					fmt.Println("Write client time_out order!")
					root.wc.SendUser(data.([]byte))
				case "driver_out":
					fmt.Println("Write client driver_out order!")
					root.wc.SendUser(data.([]byte))
				case "client_out":
					fmt.Println("Write client client_out order!")
					root.wc.SendUser(data.([]byte))
				case "driver_ready":
					fmt.Println("Write client driver_ready order!")
					root.wc.SendUser(data.([]byte))
				case "message":
					fmt.Println("Write client message")
					root.wc.SendUser(data.([]byte))
				case "begin_trip":
					fmt.Println("Write client begin_trip order!")
					root.wc.SendUser(data.([]byte))
				case "cancel":
					fmt.Println("Write client cancel order!")
					root.wc.SendUser(data.([]byte))
				case "end":
					fmt.Println("Write client end order!")
					root.wc.SendUser(data.([]byte))
				case "offer":
					fmt.Println("Write client offer order!")
					root.wc.SendUserOffer(data.([]byte))
				case "out_list":
					idCity, err := database.TakeCityIDOnIDOrder(root.db, jsonData.Data.(map[string]interface{})["id"].(string))
					if err == nil {
						fmt.Println("Write driver out_list order! - ", jsonData.Data.(map[string]interface{})["id"].(string))
						fmt.Println("Write city - ", idCity)
						root.wd.BroadcastSendOnCity(idCity, data.([]byte))
					}
				}
			} else {
				fmt.Println("Action not found")
			}
		}
	}
}

//SAWriteOrderForDriver - socket action write order for driver
func SAWriteOrderForDriver() {

}
