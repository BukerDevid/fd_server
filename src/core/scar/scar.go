package scar

import (
	"bytes"
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"../../data/model"
	"../../utility"
	"github.com/gorilla/websocket"
	"golang.org/x/net/http2"
)

var errorSCAR = ".Error of SCAR module# "

//SCAR - keep data of connection SCAR
type SCAR struct {
	addrServer string
	pathCrt    string
	client     http.Client
	caCertPool *x509.CertPool
	tlsConfig  *tls.Config
	key        string
	token      string
	id         string
}

const (
	pongWait  = 60 * time.Second
	pingReiod = (pongWait * 9) / 10
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

type wsClient struct {
	conn *websocket.Conn
}

var wsConnect = wsClient{}

//SynchWithScar - function for keep data for sinchronization
func SynchWithScar(addrServer, pathCrt, scarKey, id string) (*SCAR, error) {
	sc := &SCAR{}
	sc.addrServer = addrServer
	sc.pathCrt = pathCrt
	sc.key = scarKey
	sc.id = id
	caCert, err := ioutil.ReadFile(sc.pathCrt)
	if err != nil {
		return sc, utility.ErrorHandler(errorSCAR+"StartWebSocketWithScar.ReadFile", err)
	}
	sc.caCertPool = x509.NewCertPool()
	sc.caCertPool.AppendCertsFromPEM(caCert)
	sc.tlsConfig = &tls.Config{
		InsecureSkipVerify: true,
		RootCAs:            sc.caCertPool,
	}
	sc.client.Transport = &http2.Transport{
		TLSClientConfig: sc.tlsConfig,
	}
	return sc, nil
}

//KeepSettingForNetwork - function keeping data for connect on WebSocekt and registration on server SCAR
func (sc *SCAR) KeepSettingForNetwork() error {
	modelK := model.ScarKey{}
	modelK.KeyForSCAR = sc.key
	modelK.ID = sc.id
	data, err := json.Marshal(modelK)
	resp, err := sc.client.Post("https://"+sc.addrServer+"/server/registration", "application/json", bytes.NewBuffer(data))
	if err != nil {
		return utility.ErrorHandler(errorSCAR+"StartWebSocketWithScar.ReadAll", err)
	}
	modelT := model.ScarToken{}
	json.NewDecoder(resp.Body).Decode(&modelT)
	fmt.Printf("Get token form SCAR: %s\n", string(modelT.Token))
	sc.token = modelT.Token
	err = resp.Body.Close()
	if err != nil {
		return utility.ErrorHandler(errorSCAR+"KeepSettingForNetwork.", err)
	}
	return nil
}

//ControlSynchronizationDataOfServerOnServerOfSCAR - Control working the WebSocket for loading a data
func (sc *SCAR) ControlSynchronizationDataOfServerOnServerOfSCAR(errCh chan<- string) {
	for {
		fmt.Println("Connect ot server  ....")
		sc.SynchronizationWithScarServer()
		time.Sleep(60 * time.Second)
	}
}

//SynchronizationWithScarServer - connect WebSocket to SCAR
func (sc *SCAR) SynchronizationWithScarServer() error {
	var err error
	wsDiler := websocket.Dialer{}
	wsDiler.TLSClientConfig = sc.tlsConfig
	wsConnect.conn, _, err = wsDiler.Dial("wss://"+sc.addrServer+"/fulldriver/synch", nil)
	if err != nil {
		return utility.ErrorHandler(errorSCAR+"Can't connect to server SCAR:\n SynchronizationWithScarServer.Upgrade", err)
	}
	fmt.Println("Connecting to SCAR:", wsConnect.conn.RemoteAddr().String())
	modelT := model.ScarToken{}
	modelT.Token = sc.token
	modelT.ID = sc.id
	data, err := json.Marshal(modelT)
	wsConnect.conn.WriteMessage(2, data)
	for {
		_, data, err := wsConnect.conn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				return utility.ErrorHandler("Disconnect SCAR...  Error:\n", err)
			}
			break
		}
		key := string(data)
		if key == "LoadAllData" {
			go sc.LoadAllDataToSCAR()
		}
	}
	return nil
}

//LoadAllDataToSCAR - Load all data on server SCAR
func (sc *SCAR) LoadAllDataToSCAR() error {
	fmt.Println("Load all data...")
	return nil
}

//TestNetwork - function keeping data for connect on WebSocekt and registration on server SCAR
func (sc *SCAR) TestNetwork() error {
	resp, err := sc.client.Get(sc.addrServer + "/server/check")
	if err != nil {
		return utility.ErrorHandler(errorSCAR+"TestNetwork", err)
	}
	// body, err := ioutil.ReadAll(resp.Body)
	var data []byte
	resp.Body.Read(data)
	fmt.Printf("Got response %d: %s %s\n", resp.StatusCode, resp.Proto, string(data))
	resp.Body.Close()
	return nil
}

//StartWebSocketWithScar - connect to server on WebSocket
func StartWebSocketWithScar() error {
	return nil
	//	http2.ClientConn()
}
